import asyncio
from datetime import datetime

import aiohttp

from services.konachan import Konachan
from services.safebooru import Safebooru
from services.yandere import Yandere


class Hook(object):
    def __init__(self, client, hook):
        self.client = client
        self.data = hook
        self.channel = None
        self.sfw_channel = None
        self.nsfw_channel = None
        self.init_channels()

    def init_channels(self):
        try:
            sfw_id, sfw_token = self.data.get('sfw_channel', {}).values()
            if sfw_id and sfw_token:
                self.sfw_channel = self.client.base_url + '/' + sfw_id + '/' + sfw_token

            nsfw_id, nsfw_token = self.data.get('nsfw_channel').values()
            if nsfw_id and nsfw_token:
                self.nsfw_channel = self.client.base_url + '/' + nsfw_id + '/' + nsfw_token

            if not self.sfw_channel and not self.nsfw_channel:
                self.client.log('error', 'hook', 'channel init: ' + 'missing channel config data')
                exit(1)
        except (AttributeError, ValueError) as e:
            self.client.log('error', 'hook', 'channel init: ' + repr(e))
            exit(1)

    async def init_services(self):
        services = [Konachan, Safebooru, Yandere]
        tasks = map(lambda s: s(self.client, self).start(), services)
        await asyncio.gather(*tasks)

    async def send_discord_message(self, embed, is_nsfw, service_name):
        try:
            async with aiohttp.ClientSession() as session:
                if is_nsfw or not self.sfw_channel:
                    hook_url = self.nsfw_channel
                else:
                    hook_url = self.sfw_channel
                await session.post(hook_url, json={'embeds': [embed.to_dict()]})
        except Exception as e:
            self.client.log('error', service_name, 'sending: ' + repr(e))
            return True

    def is_nsfw(self, post):
        nsfw = False
        for tag in post.get('tags', '').split():
            if tag in self.data.get('nsfw_tags', []):
                nsfw = True
                break

        if post.get('nsfw'):
            nsfw = True
        elif post.get('rating') in ['q', 'e']:
            nsfw = True

        return nsfw
