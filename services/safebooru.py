import asyncio
import secrets

import discord
from lxml import html

API_BASE = 'http://safebooru.org/index.php?page=dapi&s=post&q=index&limit=1000&tags='
SHOW_URL = 'http://safebooru.org/index.php?page=post&s=view&id='
ICON_URL = 'https://i.imgur.com/2N0peGd.png'


class Safebooru(object):
    def __init__(self, client, hook):
        self.client = client
        self.hook = hook
        self.name = 'safebooru'
        self.exclude = self.hook.data.get('exclude_tags', [])
        self.api_url = API_BASE + '+'.join(['-' + tag for tag in self.exclude])

    async def start(self):
        if self.name not in self.hook.data.get('enabled', []):
            return

        while True:
            try:
                posts = html.fromstring(await self.client.fetch(self.api_url, self.name))
                posts = [dict(ps.attrib) for ps in posts]
                posts = self.client.ensure_age(posts, self.hook.data.get('min_age'), 'change')
                posts = self.client.ensure_url(posts)
            except Exception as e:
                self.client.log('error', self.name, 'decoding: ' + repr(e))
                posts = []

            while posts:
                post = posts.pop(secrets.randbelow(len(posts)))
                if await self.client.exists(post.get('md5')):
                    self.client.log('skipped', self.name, post.get('id'))
                    continue

                await self.client.db[self.client.db.name].AutoGallery.insert_one(post)
                permalink = f'{SHOW_URL}{post.get("id")}'
                title = f'{self.name.title()}: {post.get("id")}'
                embed = discord.Embed(color=0xad3d3d)
                embed.set_author(name=title, icon_url=ICON_URL, url=permalink)
                embed.set_image(url=post.get('file_url'))

                err = await self.hook.send_discord_message(embed, self.hook.is_nsfw(post), self.name)
                if not err:
                    self.client.log('posted', self.name, post.get('id'))

                await asyncio.sleep(self.hook.data.get('interval'))
            await asyncio.sleep(300)
