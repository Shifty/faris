import asyncio
import errno
from datetime import datetime

import aiohttp
import yaml
from motor import motor_asyncio as motor
from pymongo.errors import OperationFailure, ServerSelectionTimeoutError

from services.hook import Hook

with open('config.yml', encoding='utf-8') as config_file:
    config = yaml.safe_load(config_file)


class Database(motor.AsyncIOMotorClient):
    def __init__(self, client, cfg):
        self.get_io_loop = asyncio.get_running_loop
        self.client = client
        self.host = cfg.get('host')
        self.port = cfg.get('port')
        self.name = cfg.get('name')
        self.username = cfg.get('username')
        self.password = cfg.get('password')
        self.db_address = f'mongodb://{self.username}:{self.password}@{self.host}:{self.port}/'
        super().__init__(self.db_address)

    async def ensure_connection(self):
        self.client.log('startup', 'database', 'connection...')
        try:
            await self[self.name].collection.find_one({})
        except ServerSelectionTimeoutError:
            self.client.log('error', 'database', 'connection timed out')
            exit(errno.ETIMEDOUT)
        except OperationFailure:
            self.client.log('error', 'database', 'connection failed')
            exit(errno.EACCES)


class Gallery(object):
    def __init__(self):
        self.cfg = config
        self.headers = {'User-Agent': 'faris gallery/1.0'}
        self.base_url = f'https://canary.discord.com/api/webhooks'
        self.hooks = self.cfg.get('hooks')
        self.db = Database(self, self.cfg.get('database'))

    async def exists(self, md5):
        return bool(await self.db[self.db.name].AutoGallery.count_documents({'md5': md5}))

    @staticmethod
    def ensure_age(posts, min_age, date_key='created_at'):
        now = int(datetime.utcnow().timestamp())
        return [p for p in posts if now > int(p.get(date_key, 0)) + min_age]

    @staticmethod
    def ensure_url(posts, url_key='file_url'):
        return [p for p in posts if p.get(url_key)]

    async def fetch(self, url, service_name):
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(url, headers=self.headers) as response:
                    data = await response.read()
        except Exception as e:
            self.log('error', service_name, 'fetching: ' + repr(e))
            data = "{}"
        return data

    @staticmethod
    def log(name, service, message, n_len=7, s_len=11):
        n_buf = n_len - len(name)
        s_buf = s_len - len(service)
        print(f'{name}{" " * n_buf} | {service}{" " * s_buf} | {message}')

    async def run(self):
        await self.db.ensure_connection()
        self.log('startup', 'gallery', 'starting...')
        tasks = map(lambda h: Hook(self, h).init_services(), self.hooks)
        await asyncio.gather(*tasks)


if __name__ == '__main__':
    gallery = Gallery()
    asyncio.run(gallery.run())
